# Sunwatcher

## Usage
```python
from sunwatcher.solarlog.solarlog import SolarLog

solar_log = SolarLog('http://10.0.0.10')

generation  = "AC: %sW, DC: %sW (%i%% efficiency, %iW Loss)" % (solar_log.power_ac, solar_log.power_dc, round(solar_log.efficiency * 100, 0), solar_log.alternator_loss)
consumption = "AC Usage: %sW (%i%% usage, %iW available)"         % (solar_log.consumption_ac, round(solar_log.usage * 100, 0), solar_log.power_available)
capacity    = "Maximum power: %sW (%i%% capacity)" % (solar_log.total_power, round(solar_log.capacity * 100, 0))

print(generation)
print(consumption)
print(capacity)
```
