# v0.2.1 - Division by 0
- Return 0 efficiency/capacity/usage if generated power / power peek is 0W.

# v0.2 - SolarLog features
- Adds 'power available' as well as 'alternator loss' to SolarLog class

# v0.1 - Initial release
- Low-level binding to SolarLog HTTP API (sunwatcher.solarlog.client)
- High-level binding to SolarLog HTTP API (sunwatcher.solarlog.solarlog)
- High-level binding to IFTTT 'Maker' Channel (sunwatcher.ifttt.maker)


